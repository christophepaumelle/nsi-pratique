---
author: Franck Chambon
title: Correspondance de Łukasiewicz (2)
tags:
  - arbre
  - chemin
  - récursivité
---
# Correspondance de Łukasiewicz (2)

Dans cet exercice, **arbre** désignera un arbre enraciné ordonné sans étiquette. Chaque arbre sera modélisé avec une liste Python.

- Une feuille (un nœud externe) sera représentée par une liste vide `[]` ; la liste de ses sous-arbres est vide.
- Un nœud interne sera représenté par la liste de ses sous-arbres donnés dans l'ordre.
- Un arbre sera donné par la représentation de sa racine.

!!! exemple "Exemples"

    ![](images/arbre_simple_1.svg) représenté en interne avec `[[[], []], [[]], []]`

    ![](images/arbre_simple_2.svg) représenté en interne avec `[[[]], [[], []], []]`

    :warning: Les deux arbres ci-dessus sont différents.

    ![](images/arbre_joli.svg) représenté en interne avec `[[], [[], [[], []], [], [], []], []]`


!!! info "Correspondance de Łukasiewicz"
    Pour un arbre, on note $n$ le nombre de nœuds. On rappelle qu'ici « arbre » désigna arbre enraciné, ainsi $n>0$.

    On appelle « chemin de Łukasiewicz » un chemin de $(0, 0)$ à $(n-1, 0)$ situé dans le cadran supérieur droite (abscisses et ordonnées positives), dont les étapes sont de la forme $(1, k)$ avec $k=-1$ ou $k\in \mathbb N$.

    
    Un exercice précédent consistait à donner le chemin de Łukasiewicz associé à un arbre via une correspondance.

    L'objectif de cet exercice est de donner l'arbre associé à un chemin de Łukasiewicz.


    ![](images/chemin_joli.svg)

    ![](images/arbre_joli.svg)

    Comment faire sur cet exemple ?

    1. Le chemin est bien dans la cadrant supérieur.
    2. Le chemin est associé à une liste de valeurs `[2, -1, 4, -1, 1, -1, -1, -1, -1, -1]`
    2. On ajoute $1$ à chaque élément, et on ajoute $0$ à la fin de la liste. On obtient `[3, 0, 5, 0, 2, 0, 0, 0, 0, 0, 0]`
    3. **La difficulté est ici ; le découpage**. Il y a 3 sous arbres à la racine.
        - Le découpage est `3, [0], [5, 0, 2, 0, 0, 0, 0, 0], [0]`.
        - Le découpage de `[5, 0, 2, 0, 0, 0, 0, 0]` est `5, [0], [2, 0, 0], [0], [0], [0]`.
        - Enfin, le découpage de `[2, 0, 0]` est `2, [0], [0]`
    4. Les `[0]` obtenus sont les feuilles de l'arbre.

    :warning: Programmer le découpage est délicat.


Écrire une fonction `chemin_vers_arbre` qui implémente l'autre partie de la correspondance de Łukasiewicz.

!!! example "Exemples"

    ![](images/chemin_ex1.svg) qui correspond à la liste `[1, -1]`, donne un arbre

    ![](images/arbre_ex1.svg) représenté en interne avec `[[], []]`.


    ```pycon
    >>> chemin_vers_arbre([1, -1])
    [[], []]
    ```

    ![](images/chemin_ex2.svg) qui correspond à la liste `[2, -1, -1, 1, -1]`, donne un arbre

    ![](images/arbre_ex2.svg) représenté en interne avec `[[], [], [[], []]]`.


    ```pycon
    >>> chemin_vers_arbre([2, -1, -1, 1, -1])
    [[], [], [[], []]]
    ```

{{ IDE('exo') }}


??? tip "Indice 1"
    Il sera utile de créer une fonction récursive qui prend une liste en paramètre, ainsi qu'une position de départ et renvoie la représentation de l'arbre indiqué à cette position, ainsi que la longueur utilisée dans la liste.

    Par exemple

    ```pycon
    >>> etape_rec([3, 0, 5, 0, 2, 0, 0, 0, 0, 0, 0], 1)
    ([], 1)
    >>> etape_rec([3, 0, 5, 0, 2, 0, 0, 0, 0, 0, 0], 3)
    ([[], []], 3)
    >>> etape_rec([3, 0, 0, 2, 0, 0], 0)
    ([[], [], [[], []]], 6)
    ```

??? tip "Indice 2"
    Cette fonction récursive pourra avoir le squelette

    ```python
    def etape_rec(temp, i):
        "Fonction récursive interne"
        if temp[i] == 0:
            return [], 1
        else:
            resultat = []
            taille_totale = 1
            for _ in range(temp[i]):
                sous_arbre, taille = etape_rec(..., ...)
                taille_totale = ...
                resultat.append(...)
        return resultat, taille_totale
    ```
